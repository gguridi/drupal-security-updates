import re
from alert import Alert
from module import Module
from pony.orm import *

class Message:
    subject = None
    body = None

    def __init__(self, message):
        self.subject = message['subject']
        self.body = self.getBody(message)

    def getBody(self, message):
        if message.is_multipart():
            return message.get_payload(0).get_payload()
        return message.get_payload()

    def getModuleName(self):
        m = re.search('(?<=project/)(.+)', self.body)
        return m.group(0)

    def getAdvisory(self):
        m = re.search('(?<=Advisory ID: )(.+)', self.body)
        return m.group(0)

    def getDrupalVersions(self):
        line = re.search('Version:.*', self.body).group(0)
        return re.findall('(\d\.x)', line)

    def getDescription(self):
        m = re.search('(DESCRIPTION((-|\n)+))(([^-]|\n)+)', self.body)
        return m.group(0).strip()

    def getVersions(self):
        m = re.search('(VERSIONS AFFECTED)((.|\n)*)SOLUTION', self.body)
        return m.group(0).strip()

    def areAllVersionsAffected(self):
        message = self.getVersions()
        return True if re.search('All versions', message) else False

    def getVersionsAffected(self):
        message = self.getVersions()
        return re.findall('(?<=prior to )(\d\.x-\d{1,2}\.\d{1,2})', message)

    def isModuleAffected(self, moduleVersion):
        versions = self.getVersionsAffected()
        for version in versions:
            version = version.strip()
            if version[:4] == moduleVersion[:4] and (int(version[5:]) > int(moduleVersion[5:])):
                return True
        return self.areAllVersionsAffected()

    def getSolution(self):
        m = re.search('(SOLUTION((-|\n)+))(([^-]|\n)+)', self.body)
        return m.group(0).strip()

    def isSecurityMessage(self):
        return True if re.search('\[Security-news\]', self.subject) else False

    def process(self):
        for drupalVersion in self.getDrupalVersions():
            if self.areAllVersionsAffected():
                self.setAlert(drupalVersion, Module.ALL_VERSIONS)
            else:
                for moduleVersion in self.getVersionsAffected():
                    if drupalVersion in moduleVersion:
                        self.setAlert(drupalVersion, moduleVersion)

    @db_session
    def setAlert(self, drupalVersion, moduleVersion):
        alert = Alert.getInstance(self.getModuleName(), drupalVersion)
        alert.version = moduleVersion
        alert.loadFromMessage(self)