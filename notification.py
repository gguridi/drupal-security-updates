import config
import smtplib
from email.mime.text import MIMEText
from theme import lookup

class Notification:

    project = None
    module = None
    alert = None

    def __init__(self, project, module, alert):
        self.project = project
        self.module = module
        self.alert = alert

    def sendNotification(self):
        if config.get('notification', 'use_sendmail'):
            self.sendViaSendmail()
        else:
            self.sendViaSMTP()

    def sendViaSMTP(self):
        s = smtplib.SMTP(config.get('email', 'smtp'), config.get('email', 'smtp_port') or 0)
        s.starttls()
        s.login(config.get('email', 'username'), config.get('email', 'password'))
        message = MIMEText(self.getBody())
        message['Subject'] = self.getSubject()
        s.sendmail(self.getFrom(), self.getTo(), message.as_string())
        s.quit()

    def sendViaSendmail(self):
        from email.mime.text import MIMEText
        from subprocess import Popen, PIPE

        msg = MIMEText(self.getBody())
        msg["From"] = self.getFrom()
        msg["To"] = self.getTo()
        msg["Subject"] = self.getSubject()
        p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
        p.communicate(msg.as_string())

    def getFrom(self):
        return config.get('email', 'username')

    def getTo(self):
        return self.project.email

    def getSubject(self):
        return self.tokenize(config.get('notification', 'subject'))

    def getBody(self):
        template = lookup.get_template('body.html')
        return template.render(project = self.project, module = self.module, alert = self.alert)

    def tokenize(self, text):
        text = str(text)
        return text.format(
            project = self.project.name,
            module = self.module.getName(),
            message = self.alert.message,
        )
