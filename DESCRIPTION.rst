Drupal: Security Notifier project
=======================

This project has been designed to work with the drupal security newsletter, to provide an accurate
report of problems that your current websites can have.

To configure the service, install a local copy of the settings.cfr example file, named settings.local.cfg
Populate the email associated with the drupal newsletters, and the email that will be notified when
a project gets a security breach.
