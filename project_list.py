import config
import subprocess, os, json, datetime
from pony.orm import *
from project import Project
from subprocess import *


class ProjectList():

    def getList(self):
        return config.items('projects')

    @db_session
    def processProject(self, name, info):
        isSecure = True
        project = Project.getInstance(name, info)
        project.message = "Responsible: <strong>" + project.responsible + "</strong><br/>"
        modules = project.getModules()
        for name, module in modules.iteritems():
            isSecure = False
            alert = project.processModule(module)
            project.message += alert.message
        project.changeSecurity(isSecure)
        commit()