from pony.orm import *
from entities import db
from module import Module
from notification import Notification
import datetime
import config


class Alert(db.Entity, Module):
    label = Optional(str, default='')
    project = Required(str)
    module = Required(str)
    type = Required(str, default='module')
    version = Required(str)
    message = Required(LongStr)
    ignore = Required(int, default=0)
    added = Required(datetime.datetime, sql_default='0')
    PrimaryKey(project, module)

    @staticmethod
    def getInstance(project, module):
        """Loads alert data from the message.
        :type param Module: module to load the parameters from.
        """
        try:
            alert = Alert[project.getName(), module.getName()]
        except ObjectNotFound:
            alert = Alert(project=project.getName(), module=module.getName(), type=module.getType(), version=module.getLatestVersion(), message='Initialized', ignore=0)
        alert.loadMessage(project, module)
        if Alert.mustNotify(alert, module):
            notification = Notification(project, module, alert)
            notification.sendNotification()
        alert.version = module.getLatestVersion()
        alert.added = Alert.getNow()
        commit()
        return alert

    @staticmethod
    def mustNotify(alert, module):
        if alert.ignore:
            return False
        if config.get('notification', 'only_once') is True:
            is_new_alert = alert.added is None
            is_version_increased = alert.version != module.getLatestVersion()
            return is_new_alert or is_version_increased
        return True

    def getAdded(self):
        if self.added is None:
            return 0
        return datetime(self.added).date().toordinal()

    @staticmethod
    def getToday():
        return Alert.getNow().date().toordinal()

    @staticmethod
    def getNow():
        return datetime.datetime.now()

    def loadMessage(self, project, module):
        """Loads alert data from the message.
        :type param Module: module to load the parameters from.
        """
        self.message = module.getName() + ": Upgrade to " + module.getLatestVersion() + " "
        self.message += "<a href='" + module.getLatestVersionLink() + "'>Download the latest version</a><br/>"
