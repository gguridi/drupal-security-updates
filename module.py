import re


class Module():
    ALL_VERSIONS = 'all'

    name = ''
    data = []

    def __init__(self, key, data):
        self.name = key
        self.data = data

    def getLabel(self):
        return self.data['title']

    def getName(self):
        return self.data['name']

    def getType(self):
        return self.data['project_type']

    def getVersion(self):
        return self.data['version']

    def getLatestVersion(self):
        return self.data['latest_version']

    def getLatestVersionLink(self):
        return self.data['releases'][self.getLatestVersion()]['download_link']

    def isModule(self):
        return True if self.getType() == "module" else False