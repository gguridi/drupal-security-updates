import config
from mako.template import Template
from mako.lookup import TemplateLookup

module_path = config.get('theme', 'module')
theme = config.get('theme', 'name') or 'default'
lookup = TemplateLookup(directories=['templates/' + theme], module_directory=module_path)
