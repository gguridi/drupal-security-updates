import config
import subprocess, os, json, datetime, re
from module import Module
from alert import Alert
from notification import Notification
from pony.orm import *
from entities import db

class Project(db.Entity):
    id = PrimaryKey(str, auto=False)
    name = Required(str)
    path = Required(str)
    responsible = Required(str)
    email = Required(str)
    secure = Required(bool, default=True)
    message = Optional(LongStr)
    drush_command = Optional(str)
    drush_alias = Optional(str)

    @staticmethod
    def getInstance(key, info):
        try:
            project = Project[key]
        except ObjectNotFound:
            project = Project(id=key, name=info['name'], path=info['details']['location'], responsible=info['details']['responsible'], email=info['details']['email'])
        project.drush_command = info['details'].get('drush_command', config.get('drush', 'command'))
        project.drush_alias = info['details'].get('drush_alias', '')
        return project

    def getName(self):
        return self.name

    def getPath(self):
        return self.path

    def getSecure(self):
        return self.secure

    def changeSecurity(self, secure):
        self.secure = 1 if secure else 0

    def getModulesFromDrush(self):
        lines = []
        command = self.getDrushCall()
        process = subprocess.Popen(command, bufsize=0, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, error = process.communicate()
        output = re.sub('^[^{]*', '', str(output))
        output = re.sub('[^}]*$', '', str(output))
        output = output or "{}"
        return json.loads(output)

    def getDrushCall(self):
        call = []
        call.append(config.get('drush', 'php'))
        call.append(self.drush_command)
        if self.drush_alias:
            return [config.get('drush', 'php'), self.drush_command, '@' + str(self.drush_alias), 'pm-updatestatus', '--security-only', '--format=json']
        return [config.get('drush', 'php'), self.drush_command, '-r', str(self.path), 'pm-updatestatus', '--security-only', '--format=json']

    def getModules(self):
        modules = {}
        information = self.getModulesFromDrush()
        for name, data in information.iteritems():
            module = Module(name, data)
            modules[name] = module
        return modules

    def processModule(self, module):
        return Alert.getInstance(self, module)