import config
from project_list import ProjectList
from mail import Mail
from report import Report
from project import Project

def initialize():
    from entities import db
    db.bind(config.get('database', 'driver'),
        user = config.get('database', 'username'),
        password = config.get('database', 'password'),
        host = config.get('database', 'host'),
        database = config.get('database', 'name')
    )
    db.generate_mapping(create_tables=True)

if __name__ == '__main__':
    initialize()

    list = ProjectList()
    projects = list.getList()
    for name, details in projects.iteritems():
        list.processProject(name, details)

    report = Report()
    report.process(projects)



