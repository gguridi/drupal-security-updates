import config
import poplib, re
from email import parser
from message import Message
from email.mime.text import MIMEText

class Mail:

    connection = None

    def __init__(self):
        self.connection = poplib.POP3_SSL(config.get('email', 'pop'))
        self.connection.user(config.get('email', 'username'))
        self.connection.pass_(config.get('email', 'password'))

    def __exit__(self, type, value, traceback):
        self.connection.quit()

    def getMessages(self):
        list = []
        # Get messages from server:
        messages = [self.connection.retr(i) for i in range(1, len(self.connection.list()[1]) + 1)]
        # Concat message pieces:
        messages = ["\n".join(mssg[1]) for mssg in messages]
        #Parse message intom an email object:
        messages = [parser.Parser().parsestr(mssg) for mssg in messages]
        for item in messages:
            message = Message(item)
            if message.isSecurityMessage():
                list.append(message)
        return list



