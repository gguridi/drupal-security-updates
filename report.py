from __future__ import print_function
import config
from project import Project
from pony.orm import *
from theme import lookup


class Report:

    @db_session
    def process(self, projects):
        list = []
        for name, details in projects.iteritems():
            list.append(Project[name])
        self.serveTemplate('base.html', data = list)

    def serveTemplate(self, templatename, *args, **kwargs):
        template = lookup.get_template(templatename)
        with open(config.get('theme', 'report'), 'w') as f:
            f.write(template.render(*args, **kwargs))